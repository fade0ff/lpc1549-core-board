/*
===============================================================================
 Name        : LPC1549_PinTest.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "systime.h"
#include "systick.h"

/* Exclude P0.19 (SWCLK) and P0.20 (SWDIO) */
#define PORT0_MASK ((1<<7)|(1<< 6)|(1<< 5)|(1<< 4)|(1<< 3)|(1<< 2)|(1<< 1)|(1<< 0)| \
                   (1<<15)|(1<<14)|(1<<13)|(1<<12)|(1<<11)|(1<<10)|(1<< 9)|(1<< 8)| \
                   (1<<23)|(1<<22)|(1<<21)|(0<<20)|(0<<19)|(1<<18)|(1<<17)|(1<<16)| \
                   (1<<31)|(1<<30)|(1<<29)|(1<<28)|(1<<27)|(1<<26)|(1<<25)|(1<<24))


/** Millisecond counter */
volatile u32 ms_counter;
/** Pin Counter */
volatile u32 pin_counter;

/** 1ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1ms(u32 dummy) {
	(void)dummy;

	if (pin_counter < 32) {
		LPC_GPIO->CLR[0] = 1<<pin_counter;
	} else  if (pin_counter == 99) {
		// switch on all configured pins every 100ms
		pin_counter = -1;
		LPC_GPIO->PIN[0] = PORT0_MASK;
	}
	pin_counter++;
}

/** 10ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_10ms(u32 dummy) {
	(void)dummy;
	// currently unused
}

/** 100ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_100ms(u32 dummy) {
	(void)dummy;
}

/** 1000ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1000ms(u32 dummy) {
	(void)dummy;
	// currently unused
}

void SysTick_Handler(void) {
	PrioWrap_Function(task_1ms, 0);
	if (ms_counter % 10 == 0)
		PrioWrap_Function(task_10ms, 0);
	if (ms_counter % 100 == 0)
		PrioWrap_Function(task_100ms, 0);
	if (ms_counter % 1000 == 0)
		PrioWrap_Function(task_1000ms, 0);

	ms_counter++;
}


int main(void) {
	SystemInit();
	// NOTE: this is also meant for LQFP48 - has to be reworked for LQFP66 and LQFP100

	//PIN_Init();
	// Switch RESET to GPIO, keep only SWCLK and SWDIO

	LPC_SWM->PINENABLE[0]    = (PIN_PE0_ADC0_0(1)    | PIN_PE0_ADC0_1(1)    | PIN_PE0_ADC0_2(1)    | PIN_PE0_ADC0_3(1)    | PIN_PE0_ADC0_4(1)   | \
								PIN_PE0_ADC0_5(1)    | PIN_PE0_ADC0_6(1)    | PIN_PE0_ADC0_7(1)    | PIN_PE0_ADC0_8(1)    | PIN_PE0_ADC0_9(1)   | \
								PIN_PE0_ADC0_10(1)   | PIN_PE0_ADC0_11(1)   | PIN_PE0_ADC1_0(1)    | PIN_PE0_ADC1_1(1)    | PIN_PE0_ADC1_2(1)   | \
								PIN_PE0_ADC1_3(1)    | PIN_PE0_ADC1_4(1)    | PIN_PE0_ADC1_5(1)    | PIN_PE0_ADC1_6(1)    | PIN_PE0_ADC1_7(1)   | \
								PIN_PE0_ADC1_8(1)    | PIN_PE0_ADC1_9(1)    | PIN_PE0_ADC1_10(1)   | PIN_PE0_ADC1_11(1)   | PIN_PE0_DAC_OUT(1)  | \
								PIN_PE0_ACMP_I1(1)   | PIN_PE0_ACMP_I2(1)   | PIN_PE0_ACMP0_I3(1)  | PIN_PE0_ACMP0_I4(1)  | PIN_PE0_ACMP1_I3(1) | \
								PIN_PE0_ACMP1_I4(1)  | PIN_PE0_ACMP2_I3(1));

	LPC_SWM->PINENABLE[1]    = (PIN_PE1_ACMP2_I4(1)  | PIN_PE1_ACMP3_I3(1)  | PIN_PE1_ACMP3_I4(1)  | PIN_PE1_I2C0_SDA(1)  | PIN_PE1_I2C0_SCL(1)  | \
								PIN_PE1_SCT0_OUT3(1) | PIN_PE1_SCT0_OUT4(1) | PIN_PE1_SCT0_OUT5(1) | PIN_PE1_SCT0_OUT6(1) | PIN_PE1_SCT0_OUT7(1) | \
								PIN_PE1_SCT1_OUT3(1) | PIN_PE1_SCT1_OUT4(1) | PIN_PE1_SCT1_OUT5(1) | PIN_PE1_SCT1_OUT6(1) | PIN_PE1_SCT1_OUT7(1) | \
								PIN_PE1_SCT2_OUT3(1) | PIN_PE1_SCT2_OUT4(1) | PIN_PE1_SCT2_OUT5(1) | PIN_PE1_SCT3_OUT3(1) | PIN_PE1_SCT3_OUT4(1) | \
								PIN_PE1_SCT3_OUT5(1) | PIN_PE1_RESETN(1)    | PIN_PE1_SWCLK(0)     | PIN_PE1_SWDIO(0));

	LPC_SWM->PINASSIGN[0]  = (PIN_PA0_UART0_TXD(PIN_NA)        | PIN_PA0_UART0_RXD(PIN_NA)       | PIN_PA0_UART0_RTS(PIN_NA)       | PIN_PA0_UART0_CTS(PIN_NA));
	LPC_SWM->PINASSIGN[1]  = (PIN_PA1_UART0_SCLK(PIN_NA)       | PIN_PA1_UART1_TXD(PIN_NA)       | PIN_PA1_UART1_RXD(PIN_NA)       | PIN_PA1_UART1_RTS(PIN_NA));
	LPC_SWM->PINASSIGN[2]  = (PIN_PA2_UART1_CTS(PIN_NA)        | PIN_PA2_UART1_SCLK(PIN_NA)      | PIN_PA2_UART2_TXD(PIN_NA)       | PIN_PA2_UART2_RXD(PIN_NA));
	LPC_SWM->PINASSIGN[3]  = (PIN_PA3_UART2_SCLK(PIN_NA)       | PIN_PA3_SPI0_SCK(PIN_NA)        | PIN_PA3_SPI0_MOSI(PIN_NA)       | PIN_PA3_SPI0_MISO(PIN_NA));
	LPC_SWM->PINASSIGN[4]  = (PIN_PA4_SPI0_SSELSN_0(PIN_NA)    | PIN_PA4_SPI0_SSELSN_1(PIN_NA)   | PIN_PA4_SPI0_SSELSN_2(PIN_NA)   | PIN_PA4_SPI0_SSELSN_3(PIN_NA));
	LPC_SWM->PINASSIGN[5]  = (PIN_PA5_SPI1_SCK(PIN_NA)         | PIN_PA5_SPI1_MOSI(PIN_NA)       | PIN_PA5_SPI1_MISO(PIN_NA)       | PIN_PA5_SPI1_SSELSN_0(PIN_NA));
	LPC_SWM->PINASSIGN[6]  = (PIN_PA6_SPI1_SSELSN_1(PIN_NA)    | PIN_PA6_CAN_TD1(PIN_NA)         | PIN_PA6_SPI1_CAN_RD1(PIN_NA)    | PIN_PA6_USB_CONNECT(PIN_NA));
	LPC_SWM->PINASSIGN[7]  = (PIN_PA7_USB_VBUS(PIN_NA)         | PIN_PA7_SCT0_OUT0(PIN_NA)       | PIN_PA7_SCT0_OUT1(PIN_NA)       | PIN_PA7_SCT0_OUT2(PIN_NA));
	LPC_SWM->PINASSIGN[8]  = (PIN_PA8_SCT1_OUT0(PIN_NA)        | PIN_PA8_SCT1_OUT1(PIN_NA)       | PIN_PA8_SCT1_OUT2(PIN_NA)       | PIN_PA8_SCT2_OUT0(PIN_NA));
	LPC_SWM->PINASSIGN[9]  = (PIN_PA9_SCT2_OUT1(PIN_NA)        | PIN_PA9_SCT2_OUT2(PIN_NA)       | PIN_PA9_SCT3_OUT0(PIN_NA)       | PIN_PA9_SCT3_OUT1(PIN_NA));
	LPC_SWM->PINASSIGN[10] = (PIN_PA10_SCT3_OUT2(PIN_NA)       | PIN_PA10_SCT_ABORT0(PIN_NA)     | PIN_PA10_SCT_ABORT1(PIN_NA)     | PIN_PA10_ADC0_PIN_TRIG0(PIN_NA));
	LPC_SWM->PINASSIGN[11] = (PIN_PA11_ADC0_PIN_TRIG1(PIN_NA)  | PIN_PA11_ADC1_PIN_TRIG0(PIN_NA) | PIN_PA11_ADC1_PIN_TRIG1(PIN_NA) | PIN_PA11_DAC_PIN_TRIG(PIN_NA));
	LPC_SWM->PINASSIGN[12] = (PIN_PA12_DAC_SHUTOFF(PIN_NA)     | PIN_PA12_ACMP0_OUT(PIN_NA)      | PIN_PA12_ACMP1_OUT(PIN_NA)      | PIN_PA12_ACMP2_OUT(PIN_NA));
	LPC_SWM->PINASSIGN[13] = (PIN_PA13_ACMP3_OUT(PIN_NA)       | PIN_PA13_CLK_OUT(PIN_NA)        | PIN_PA13_ROSC0(PIN_NA)          | PIN_PA13_ROSC_RST0(PIN_NA));
	LPC_SWM->PINASSIGN[14] = (PIN_PA14_USB_FRAME_TOG(PIN_NA)   | PIN_PA14_QEI0_PHA(PIN_NA)       | PIN_PA14_QEI0_PHB(PIN_NA)       | PIN_PA14_QEI0_IDX(PIN_NA));
	LPC_SWM->PINASSIGN[15] = (PIN_PA15_GPIO_INT_BMATCH(PIN_NA) | PIN_PA15_SWO(PIN_NA));

	// Configure all pins as output
	LPC_GPIO->DIR[0] = PORT0_MASK;
	// switch all configured pins on
	LPC_GPIO->PIN[0] = PORT0_MASK;

	SYSTIME_Init();                         /* init system timer */

	// Init timebase
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	PrioWrap_Init();
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 1ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);

    while(1) {

    }

}

