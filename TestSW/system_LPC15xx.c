/******************************************************************************
 * @file     system_LPC15xx.c
 * @purpose  CMSIS Cortex-M3 Device Peripheral Access Layer Source File
 *           for the NXP LPC15xx Device Series
 * @version  V1.10
 * @date     24. November 2010
 *
 * @note
 * Copyright (C) 2009-2010 ARM Limited. All rights reserved.
 *
 * @par
 * ARM Limited (ARM) is supplying this software for use with Cortex-M 
 * processor based microcontrollers.  This file can be freely distributed 
 * within development tools that are supporting such ARM based processors. 
 *
 * @par
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * ARM SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 ******************************************************************************/


#include <stdint.h>
#include "LPC15xx.h"
#include "sys.h"

/*----------------------------------------------------------------------------
  DEFINES
 *----------------------------------------------------------------------------*/
#define SYS_SetFlashConfig(c) {LPC_SYSCTL->FLASHCFG |= (((c) & 0x03) << 12);}
    
/*----------------------------------------------------------------------------
  Clock Variable definitions
 *----------------------------------------------------------------------------*/
uint32_t SystemCoreClock = SYS_FRQ_CORE;  ///! System Clock Frequency (Core Clock)

/*----------------------------------------------------------------------------
  Clock functions
 *----------------------------------------------------------------------------*/
/* Update system core clock rate, should be called if the system has
   a clock rate change */
void SystemCoreClockUpdate(void) {
	uint32_t clkRate;

	uint32_t clkbsel = LPC_SYSCTL->MAINCLKSELB;
	
	if (clkbsel == 0 /* MAINCLKSELA */) {
		switch (LPC_SYSCTL->MAINCLKSELA) {
			case SYSCTL_MAINCLKSELA_IRC:
				clkRate = SYSCTL_IRC_FREQ;
				break;
			case SYSCTL_MAINCLKSELA_SYSOSC:
				clkRate = SYS_FRQ_OSC;
				break;
			case SYSCTL_MAINCLKSELA_WDOSC:
				clkRate = SYSCTL_WDTOSC_FREQ;
				break;
			default:
				clkRate = 0;
				break;
		}
	
	} else {
		switch (clkbsel) {
			case 1: /* PLL_IN */
				if (LPC_SYSCTL->SYSPLLCLKSEL == SYSCTL_PLLCLKSRC_IRC)
					clkRate = SYSCTL_IRC_FREQ;
				else
					clkRate = SYS_FRQ_OSC;
				break;
			case 2: /* PLL_OUT */
				if (LPC_SYSCTL->SYSPLLCLKSEL == SYSCTL_PLLCLKSRC_IRC)
					clkRate = SYSCTL_IRC_FREQ;
				else
					clkRate = SYS_FRQ_OSC;
				clkRate *=	((LPC_SYSCTL->SYSPLLCTRL & 0x3F) + 1);
				break;
			case 3: /* RTC  */
				clkRate = SYSCTL_RTCOSC_FREQ;
				break;
			default:
				clkRate = 0;
				break;
		}
	}
	/* CPU core speed */
	SystemCoreClock = clkRate / LPC_SYSCTL->SYSAHBCLKDIV;
}

/* Set up and initialize hardware prior to call to main */
void SystemInit(void) {
	int i;

	/* Initial internal clocking */
#if SYS_FRQ_USE_IRC
	/* Power up main IRC (likely already powered up) */
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_IRC);
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_IRCOUT_PD);
#endif

#if SYS_FRQ_USE_SYSOSC
	/* Power up main oscillator */
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_SYSOSC_PD);
#endif

	/* Set system clock divider */
	SYSCTL_SetSysClockDiv(SYS_CLK_DIV);

	/* Setup FLASH access timing */
	if (SYS_FRQ_CORE <= 25000000UL) {
		SYS_SetFlashConfig(0);
	} else if (SYS_FRQ_CORE <= 55000000UL) {
		SYS_SetFlashConfig(1);
	} else
		SYS_SetFlashConfig(2);

#if SYS_FRQ_USE_SYS_PLL
	/* Set system PLL input to IRC */
	SYSCTL_SYSPLL_SetSource(SYS_PLL_CLKSRC_CFG);

	/* Power down PLL to change the PLL divider ratio */
	SYSCTL_PowerDown(SYSCTL_POWERDOWN_SYSPLL_PD);

	/* Setup PLL for main oscillator  */
	LPC_SYSCTL->SYSPLLCTRL = SYSPLLCFG_CFG;

	/* Powerup system PLL */
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_SYSPLL_PD);

	/* Wait for PLL to lock */
	for (i = 0; i < 256; i++)
		__NOP();
	while (!SYSCTL_SYSPLL_Locked()) {}
#endif

#if SYS_MAIN_CLKSEL_CFG <= SYSCTL_MAINCLKSELA_WDOSC
	LPC_SYSCTL->MAINCLKSELA = SYS_MAIN_CLKSEL_CFG;
	LPC_SYSCTL->MAINCLKSELB = 0; /* CLKSELA */
#else
	LPC_SYSCTL->MAINCLKSELB = SYS_MAIN_CLKSEL_CFG-SYSCTL_MAINCLKSELA_WDOSC;
#endif

#if SYS_FRQ_USE_USB_PLL
	/* Set USB PLL input to main oscillator */
	SYSCTL_USBPLL_SetSource(SYS_USBPLL_CLKSRC_CFG);
	/* Setup USB PLL */
	LPC_SYSCTL->USBPLLCTRL = USBPLLCFG_CFG;

	/* Power up USB PLL */
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_USBPLL_PD);

	/* Wait for PLL to lock */
	while (!SYSCTL_USBPLL_Locked()) {}
#endif

#if SYS_FRQ_USE_SCT_PLL
	/* Set USBSCTPLL input to main oscillator */
	SYSCTL_SCTPLL_SetSource(SYS_SCTPLL_CLKSRC_CFG);
	/* Setup SCT PLL */
	LPC_SYSCTL->SCTPLLCTRL = SCTPLLCFG_CFG;

	/* Power up SCT PLL */
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_SCTPLL_PD);

	/* Wait for PLL to lock */
	while (!SYSCTL_SCTPLL_Locked()) {}
#endif

	/* setup peripheral clock selects */
	LPC_SYSCTL->USBCLKSEL = SYS_CLK_USB_CLKSEL_CFG;
	LPC_SYSCTL->ADCASYNCCLKSEL = SYS_CLK_ADCASYNC_CLKSEL_CFG;
#if SYS_CLK_CLKOUT_CLKSEL_CFG <= SYSCTL_CLKOUTSELA_MAIN
	LPC_SYSCTL->CLKOUTSEL[0] = SYS_CLK_CLKOUT_CLKSEL_CFG;
	LPC_SYSCTL->CLKOUTSEL[1] = 0; /* CLKSELA */
#else
	LPC_SYSCTL->CLKOUTSEL[1] = SYS_CLK_CLKOUT_CLKSEL_CFG-SYSCTL_CLKOUTSELA_MAIN;
#endif

	/* setup peripheral clock dividers */
	LPC_SYSCTL->SYSTICKCLKDIV = SYS_CLK_SYSTICK_DIV;
	LPC_SYSCTL->UARTCLKDIV = SYS_CLK_UART_DIV;
	LPC_SYSCTL->TRACECLKDIV = SYS_CLK_TRACE_DIV;
	LPC_SYSCTL->IOCONCLKDIV = SYS_CLK_IOCON_DIV;
	LPC_SYSCTL->USBCLKDIV = SYS_CLK_USB_DIV;
	LPC_SYSCTL->ADCASYNCCLKDIV = SYS_CLK_ADC_AYSNC_DIV;
	LPC_SYSCTL->CLKOUTDIV = SYS_CLK_CLKOUT_DIV;

	/* System clock to the IOCON needs to be enabled or
	most of the I/O related peripherals won't work. */
	LPC_SYSCON->SYSAHBCLKCTRL[0] |= SYSAHBCLKCTRL0_CFG;
	LPC_SYSCON->SYSAHBCLKCTRL[1] |= SYSAHBCLKCTRL1_CFG;
}
